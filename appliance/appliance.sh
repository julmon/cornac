#!/bin/bash -eux
#
# Builds a VM called cornac-appliance, using libvirt.
#

set -o pipefail
shopt -qs nullglob

find_location() {
	local ips=$(hostname --all-ip-addresses)
	ips=($ips)
	echo http://${ips[0]}:8000/installer
}

# Commented list of boot cmdline.
boot_cmdline=(
	quiet
	console=ttyS0
	ip=dhcp
	plymouth.enable=0
	# Comment this to re-enable emergency shell on error.
	rd.shell=0 rd.emergency=halt

	# Limit dracut timeout since we're on virtual system.
	rd.retry=3 rd.timout=15

	# Define Kickstart script.
	ks=file:/cornac.ks

	# For anaconda
	inst.cmdline
	inst.headless
)

if [ -n "${http_proxy-}" ] ; then
	boot_cmdline+=(proxy=${http_proxy})
fi

# Commented list of virt-install arguments.
virtinstall_args=(
	--debug

	# Various system specs.
	--name cornac-appliance
	--os-type linux
	--os-variant centos7.0
	--features acpi=on,apic=on
	--vcpus 2
	# This is the minimum for anaconda to run properly.
	--ram 2048
	--memballoon virtio

	--network type=user

	# Enable graphics, but use console for headless installation.
	--console pty,target_type=serial
	--graphics spice
	--video qxl
	--channel spicevmc

	# Add 4G disk on a paravirt SCSI bus
	--disk bus=scsi,size=8,format=qcow2,sparse=yes
	--controller scsi,model=virtio-scsi

	# Download installer and packages instead of using an ISO.
	--location http://mirror.i3d.net/pub/centos/7/os/x86_64/

	# Inject kickstart script.
	--initrd-inject "${PWD}/cornac.ks"

	# Ensure VM is down after installation.
	--noreboot

	--extra-args "${boot_cmdline[*]}"

	# Return right after VM is launched we'll stream installation output
	# from text console.
	--noautoconsole

)

# Trigger installation
virt-install "${virtinstall_args[@]}"

# Watch installation in TTY, wait for shutdown.
virsh console cornac-appliance --force

# Guestfs tools needs SUDO privileges to manage system images.
uri=$(virsh uri)
sudo=
if [ -z "${uri#*/system}" ] ; then
    sudo=sudo
fi

# Show and check post install script success.
$sudo virt-cat --domain cornac-appliance /var/log/cornac-kickstart-post.log | tee /dev/stderr | grep -q SUCCESS

copies=()
install=()
for f in custom/*.rpm ; do
	copies+=(--copy-in $f:/tmp/custom)
	install+=(/tmp/custom/${f##*/})
done
install="${install[*]}"
if [ "${#copies[@]}" -gt 0 ] ; then
	# Install custom packages.
	$sudo virt-sysprep \
		--domain cornac-appliance \
		--network \
		--mkdir /tmp/custom \
		"${copies[@]}" \
		--install "${install// /,}" \
		--delete /tmp/custom \
		--selinux-relabel \
		${NULL-}
fi

# Export compress disk as vmdk for ova.
diskfile=$(virsh dumpxml cornac-appliance | grep -Po "file='\K.*/cornac-appliance.qcow2")
$sudo virt-sparsify $diskfile --convert vmdk -o adapter_type=lsilogic,subformat=streamOptimized,compat6 cornac-sda.vmdk
