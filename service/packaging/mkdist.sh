#!/bin/bash -eux

top_srcdir=$(readlink -m $0/../..)
cd $top_srcdir

# Test for build requirements.
test -f pyproject.toml
type -p poetry

mkdir -p ${XDG_CACHE_DIR-~/.cache}
chown -R $(id -nu): ${XDG_CACHE_DIR-~/.cache}

files=($(poetry build | tee /dev/stderr | grep -Po 'Built \KpgCornac-.*'))

# Now, symlink -last dist so that mkrpm.sh will safely defaults to latest build.
#
# Poetry uses semver in pyproject.toml, but translate to PEP440 version in final
# dist artefacts. Thus, we extract PEP440 version from wheel filename where
# version is surrounded by hyphens.
#
# See. https://github.com/sdispater/poetry/issues/1389
whl=${files[1]}
suffix=${whl#pgCornac-}
version440=${suffix%%-*.whl}
ownership=$(stat -c %u:%g pyproject.toml)
for file in ${files[@]} ; do
	ln -fs $file dist/${file/-$version440*./-last.}
	chown $ownership dist/$file $_
done
