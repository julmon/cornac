#!/bin/bash -eux

dir=$(readlink -m $0/..)

$dir/reset.sh

rm -rf \
   /opt/cornac \
   /usr/local/lib/systemd/system/cornac-*.service \
   ;

systemctl daemon-reload
