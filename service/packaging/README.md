# Packaging for Linux Distribution

Only CentOS7 is supported. Execute commands through `Makefile`. You need
docker-compose, poetry >1.0.0a3 and access to yum.dalibo.org/labs

- `make centos7` builds rpm in ../dist/.
- `make push` pushes rpm in yum.dalibo.org/labs. See private documentation for
  details.
