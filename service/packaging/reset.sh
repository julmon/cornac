#!/bin/bash -eux
#
# Reset what cornac-setup and documentation do.
#

for s in cornac-{scheduler,web} cornac-worker{,@default,@sauvegardes,@maintenance} ; do
	if systemctl --quiet is-enabled $s 2>/dev/null ; then
		systemctl disable --now $s
	fi

	if systemctl --quiet is-active $s ; then
		systemctl kill $s
	fi
done

for u in cornac-web cornac-worker ; do
	if getent passwd $u >/dev/null ; then
		userdel --force --remove $u
	fi

	if getent group $u >/dev/null ; then
		groupdel $u
	fi
done

rm -rf \
   /etc/nginx/conf.d/cornac.conf \
   /etc/opt/cornac \
   /etc/pki/tls/certs/cornac.pem \
   /etc/pki/tls/private/cornac.key \
   ;

systemctl restart nginx
