def test_patch_dict():
    from cornac.utils import patch_dict

    my = dict(modified=1, unmodified=2)

    with patch_dict(my, modified=3, new=4):
        assert 2 == my['unmodified']
        assert 3 == my['modified']
        assert 4 == my['new']

    assert 1 == my['modified']
    assert 'new' not in my
