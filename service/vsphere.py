import logging

from dotenv import find_dotenv, load_dotenv

from cornac import create_app
from cornac.iaas import IaaS


logging.basicConfig(level=logging.DEBUG, format='%(levelname)1.1s %(message)s')
load_dotenv(find_dotenv())
app = create_app()

with app.app_context():
    c = app.config
    with IaaS.connect(c['IAAS'], c) as iaas:
        iaas.create_machine('toto', c['STORAGE_POOL'], data_size_gb=5)
        for vm in iaas.list_machines():
            print(vm)
