# Apply actions on infrastructure.
#
# The concept of Operator is borrowed from K8S.
#

import logging
import pdb
import sys
from pathlib import Path

from . import Operator
from ..iaas import IaaS
from ..ssh import Password, RemoteShell


logger = logging.getLogger(__name__)


class BasicOperator(Operator):
    # Implementation using pghelper.sh

    helper = '/usr/local/bin/pghelper.sh'

    def create_db_instance(self, instance):
        name = instance.data['DBInstanceIdentifier']
        machine = self.iaas.create_machine(
            name=name,
            storage_pool=self.config['STORAGE_POOL'],
            data_size_gb=instance.data['AllocatedStorage'],
        )
        self.iaas.start_machine(machine)
        address = self.iaas.endpoint(machine)
        shell = RemoteShell('root', address)
        shell.wait()
        logger.debug("Sending helper script.")
        local_helper = str(Path(__file__).parent / 'pghelper.sh')
        shell.copy(local_helper, self.helper)

        # Formatting disk
        try:
            # Check whether Postgres VG is configured.
            shell(["test", "-d", "/dev/Postgres"])
        except Exception:
            dev = self.iaas.guess_data_device_in_guest(machine)
            logger.info("Preparing disk %s.", dev)
            shell([self.helper, "prepare-disk", dev])
            logger.info("Creating Postgres instance.")
            shell([
                self.helper, "create-instance",
                instance.data['EngineVersion'],
                instance.data['DBInstanceIdentifier'],
            ])
            shell([self.helper, "start"])
        else:
            logger.info("Reusing Postgres instance.")

        # Master user
        master = instance.data['MasterUsername']
        shell([
            self.helper,
            "create-masteruser", master,
            Password(instance.data['MasterUserPassword']),
        ])

        # Creating database
        bases = shell([self.helper, "psql", "-l"])
        if f"\n {name} " in bases:
            logger.info("Reusing database %s.", name)
        else:
            logger.info("Creating database %s.", name)
            shell([self.helper, "create-database", name, master])

        instance.data['Endpoint'] = dict(Address=address, Port=5432)

    def is_running(self, machine):
        # Check whether *Postgres* is running.
        address = self.iaas.endpoint(machine)
        shell = RemoteShell('root', address)
        try:
            shell([self.helper, "psql", "-l"])
            return True
        except Exception as e:
            logger.debug("Failed to execute SQL in Postgres: %s.", e)
            return False


def test_main():
    # Hard coded real test case, for PoC development.

    from flask import current_app as app

    # What aws would send to REST API.
    command = {
        'DBInstanceIdentifier': 'cli0',
        'AllocatedStorage': 5,
        'DBInstanceClass': 'db.t2.micro',
        'Engine': 'postgres',
        'EngineVersion': '11',
        'MasterUsername': 'postgres',
        'MasterUserPassword': 'C0nfidentiel',
    }

    with IaaS.connect(app.config['IAAS'], app.config) as iaas:
        operator = BasicOperator(iaas, app.config)
        response = operator.create_db_instance(command)

    logger.info(
        "    psql -h %s -p %s -U %s -d %s",
        response['Endpoint']['Address'],
        response['Endpoint']['Port'],
        command['MasterUsername'],
        command['DBInstanceIdentifier'],
    )


if "__main__" == __name__:
    from cornac import create_app

    logging.basicConfig(
        format="%(levelname)5.5s %(message)s",
        level=logging.DEBUG,
    )
    app = create_app()
    try:
        with app.app_context():
            test_main()
    except pdb.bdb.BdbQuit:
        pass
    except Exception:
        logger.exception("Uhandled error:")
        pdb.post_mortem(sys.exc_info()[2])
