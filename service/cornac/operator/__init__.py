import logging
import re
from pkg_resources import iter_entry_points
from urllib.parse import urlparse

from ..errors import KnownError
from ..ssh import RemoteShell, wait_machine


logger = logging.getLogger(__name__)


class Operator(object):
    RESERVED_ROLES = ['postgres', 'rdsadmin']

    _supg = ["sudo", "-iu", "postgres"]

    @staticmethod
    def factory(iaas, config):
        name = config['OPERATOR']
        try:
            ep, *_ = iter_entry_points('cornac.operators', name=name)
        except ValueError:
            raise KnownError(f"Unknown operator type {name}")

        cls = ep.load()
        return cls(iaas, config)

    def __init__(self, iaas, config):
        self.iaas = iaas
        self.config = config

    def delete_db_instance(self, instance):
        self.iaas.delete_machine(instance.identifier)

    def maintainance(self, instance):
        pass

    def recovery_end(self, instance):
        pass

    def setup_ssh_for_backup(self, shell, comment, backup_url):
        logger.info('Generating SSH key on Postgres host.')
        path = "/var/lib/pgsql/.ssh/id_rsa"
        shell(self._supg + [
            # Sudo -i breaks passing emtpy arguments. See
            # https://stackoverflow.com/questions/27892812/passing-empty-arguments-to-sudo-i/27892867#27892867
            "/bin/bash", "-ec",
            f"ssh-keygen -b 2048 -t rsa -C {comment} -f {path} -N ''",
        ])

        backup_url = urlparse(backup_url)
        destination = f"{backup_url.username}@{backup_url.hostname}"
        logger.info('Authorize postgres on %s.', destination)
        shell([
            "ssh-copy-id", "-i", path, destination,
        ], ssh_options=["-A"])

        # Check SSH access.
        shell(self._supg + ["ssh", destination, "true"])

    def start_db_instance(self, instance):
        self.iaas.start_machine(instance.identifier)
        wait_machine(instance.data['Endpoint']['Address'])

    def stop_db_instance(self, instance):
        self.iaas.stop_machine(instance.identifier)

    def teardown_ssh_for_backup(self, shell, comment, backup_url):
        url = urlparse(backup_url)
        pattern = re.sub(r'[ /+]', '.', comment)
        shell = RemoteShell(url.username, url.hostname)
        shell(['sed', '-i', f"/ {pattern}$/d", ".ssh/authorized_keys"])
