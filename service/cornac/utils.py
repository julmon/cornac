import string
from contextlib import contextmanager
from datetime import datetime
from random import choice
from textwrap import dedent

from flask import (
    current_app,
    url_for,
    _app_ctx_stack,
    _request_ctx_stack,
)


def parse_time(timestamp):
    return datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')


def format_time(date=None):
    date = date or datetime.utcnow()
    if date.tzinfo:
        utcoffset = date.tzinfo.utcoffset(date)
        date = date - utcoffset
    date = date.replace(tzinfo=None)
    return date.isoformat(timespec='seconds') + 'Z'


def heredoc(string):
    # Ease writing long error message by trimming indentation and unwrapping
    # lines.
    return dedent(string).replace('\n', ' ').strip()


def pwgen():
    _pwchars = string.ascii_letters + string.digits
    return ''.join([choice(_pwchars) for _ in range(32)])


@contextmanager
def patch_dict(dict_, **values):
    new = {k for k in values if k not in dict_}
    backup = {k: dict_[k] for k in values if k not in new}
    dict_.update(values)
    try:
        yield dict_
    finally:
        dict_.update(backup)
        for k in new:
            dict_.pop(k)


@contextmanager
def patch_url_adapter(adapter):
    ctx = _request_ctx_stack.top or _app_ctx_stack.top
    old = ctx.url_adapter
    ctx.url_adapter = adapter
    try:
        yield ctx.url_adapter
    finally:
        ctx.url_adapter = old


def canonical_url_for(endpoint, **kw):
    """ Alternative of url_for to build constant canonical URL """
    # We want cornac webservice to be accessible from any address, e.g. to
    # listen on 0.0.0.0. At the same time, we need to set SERVER_NAME to forge
    # URL from background worker or commands, but also to forge canonical URL
    # while queried from any IP.
    #
    # But once SERVER_NAME is set in config, Flask requires every request's
    # HTTP Host header to match SERVER_NAME. This breaks querying from
    # localhost or by IP.
    #
    # To workaround this, cornac has a dedicated CANONICAL_URL config option,
    # combining PREFERRED_URL_SCHEME, SERVER_NAME and APPLICATION_ROOT, but
    # only for URL building, not routing.
    with patch_url_adapter(current_app.canonical_url_adapter):
        return url_for(endpoint, **kw)


def update_command_defaults(command, **new_defaults):
    params = {
        p.name: p
        for p in command.params
        if p.name in new_defaults
    }
    for k, v in new_defaults.items():
        params[k].default = v
