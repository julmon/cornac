from flask import Blueprint, request, current_app
from flask.json import jsonify
from sqlalchemy.orm import exc

from .. import worker
from ..core.model import db, DBInstance


blueprint = Blueprint('cornac', __name__)


@blueprint.route('/')
def index():
    index_html = current_app.config['CONSOLE_HTDOCS'] + '/index.html'
    with open(index_html) as fo:
        html = fo.read()
    endpoint = current_app.config['CANONICAL_URL'] + '/'
    config = f'var endpoint = "{endpoint}";'
    needle = '// var endpoint = "https://prod.cornac.company.lan/";'
    return html.replace(needle, config)


@blueprint.route('/recovery-end-callback', methods=['POST'])
def recovery_end_callback():
    token = request.args['token']
    qry = DBInstance.query.filter(DBInstance.recovery_token == token).one
    try:
        instance = qry()
    except exc.NoResultFound:
        response = jsonify(message="Unknown token.")
        response.status_code = 404
        return response

    worker.recovery_end.send(instance.id)

    instance.recovery_token = None
    db.session.commit()

    return jsonify(message="Success, recovery end task queued.")
