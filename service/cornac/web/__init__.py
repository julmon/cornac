from flask import current_app, make_response, request

from .rds import blueprint as rds
from .cornac import blueprint as cornac


def set_server_header(response):
    response.headers['Server'] = current_app.app_version
    return response


def fallback(e):
    # By default, log every requests.
    current_app.logger.info(
        "Unhandled request: %s %s %s",
        request.method, request.path, dict(request.form))
    return make_response('Not Found', 404)


__all__ = ['cornac', 'fallback', 'rds']
