import logging
from functools import partial
from uuid import uuid4

from flask import Blueprint, current_app, g, request
from werkzeug.exceptions import HTTPException

from . import (
    actions,
    errors,
    xml,
)
from .auth import authenticate


blueprint = Blueprint('rds', __name__)
logger = logging.getLogger(__name__)


def log(requestid, action_name, identifier, result, code=200,
        level=logging.INFO, access_key='-'):
    # Composable helper for request logging.
    current_app.logger.log(
        level, "RDS %s %s %s %s %s",
        access_key, action_name, identifier, code, result)


# This is RDS entrypoint. It's a single POST request handler. RDS action are
# defined in POST payload.
#
# To implement a new RDS action, one must define a function in
# cornac.web.actions. If the action requires background processing or worker
# privileges, one must define a corresponding task (or actor) in cornac.worker.
# Finally, if the action require effective operation on Postgres host or in
# IaaS, one must add new methods in cornac.operator.Operator or its
# implementation, as well as cornac.iaas.IaaS and its implementation.
@blueprint.route("/", methods=["POST"])
def main():
    # Bridge RDS service and Flask routing. RDS actions are not RESTful.
    payload = dict(request.form.items(multi=False))
    identifier = payload.get('DBInstanceIdentifier', '-')
    requestid = uuid4()
    log_ = partial(
        log, requestid, payload.get('Action', '*UndefinedAction*'), identifier)

    try:
        action, payload = check_payload(payload)
        g.access_key = authenticate(request)
        log_ = partial(log_, access_key=g.access_key)

        response = xml.make_response_xml(
            action=action.__name__,
            result=action(**payload),
            requestid=requestid,
        )
        log_(result='OK')
    except HTTPException as e:
        if not isinstance(e, errors.RDSError):
            e = errors.RDSError(code=e.code, description=str(e))
        # Still log user error at INFO level.
        log_(code=e.code, result=e.rdscode)
        response = xml.make_error_xml(error=e, requestid=requestid)
    except Exception:
        # Don't expose error.
        e = errors.RDSError()
        current_app.logger.exception("Unhandled RDS error:")
        log_(code=e.code, result=e.rdscode, level=logging.ERROR)
        response = xml.make_error_xml(error=e, requestid=requestid)

    return response


def check_payload(payload):
    action_name = payload.pop('Action', None)
    if action_name is None:
        raise errors.InvalidAction()

    version = payload.pop('Version', None)
    if version != '2014-10-31':
        raise errors.InvalidAction(
            description=f"Unsupported API version {version}.")

    action = getattr(actions, action_name, None)
    if action is None:
        logger.warning("Unknown RDS action: %s.", action_name)
        logger.debug("payload=%r", payload)
        raise errors.InvalidAction()
    action.__name__ = action_name

    return action, payload
