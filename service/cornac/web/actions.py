# RDS-like service.
#
# Each method corresponds to a well-known RDS action, returning result as
# XML snippet.

import logging
from uuid import uuid4

from flask import current_app
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from . import (
    errors,
    xml,
)
from .. import worker
from ..core.model import DBInstance, DBSnapshot, db
from ..operator import Operator
from ..utils import heredoc


logger = logging.getLogger(__name__)


def get_instance(identifier, status=None):
    try:
        instance = (
            DBInstance.query
            .filter(DBInstance.identifier == identifier)
            .one())
    except NoResultFound:
        raise errors.DBInstanceNotFound(identifier)
    check_status(instance, status)
    return instance


def get_snapshot(identifier, status=None):
    try:
        snapshot = (
            DBSnapshot.query
            .filter(DBSnapshot.identifier == identifier)
            .one())
    except NoResultFound:
        raise errors.DBSnapshotNotFound(identifier)
    check_status(snapshot, status)
    return snapshot


def check_create_command(command):
    command['AllocatedStorage'] = int(command['AllocatedStorage'])
    command['MultiAZ'] = command.get('MultiAZ', 'false') == 'true'
    if command['MultiAZ']:
        raise errors.InvalidParameterCombination(
            "Multi-AZ instance is not yet supported.")
    return command


def CreateDBInstance(**command):
    command = check_create_command(command)

    if command['MasterUsername'] in Operator.RESERVED_ROLES:
        raise errors.InvalidParameterValue(
            f"MasterUsername {command['MasterUsername']} cannot be used as it "
            "is a reserved word used by the engine")

    identifier = check_instance_identifier(command['DBInstanceIdentifier'])
    instance = DBInstance.factory(identifier)
    instance.data.update(command)
    db.session.add(instance)
    try:
        db.session.commit()
    except IntegrityError:
        raise errors.DBInstanceAlreadyExists()

    worker.create_db_instance.send(instance.id)

    return xml.InstanceEncoder(instance).as_xml()


def CreateDBSnapshot(*, DBInstanceIdentifier, **command):
    instance = get_instance(DBInstanceIdentifier)
    check_status(instance, msg=heredoc(f"""\
    Cannot create a snapshot because the database instance
    {DBInstanceIdentifier} is not currently in the available state.
    """))

    identifier = check_snapshot_identifier(command['DBSnapshotIdentifier'])
    snapshot = DBSnapshot.factory(instance, 'manual', identifier)
    snapshot.data.update(command)
    db.session.add(snapshot)
    try:
        db.session.commit()
    except IntegrityError:
        raise errors.DBSnapshotAlreadyExists(identifier=snapshot.identifier)

    worker.create_db_snapshot.send(snapshot.id)

    return xml.SnapshotEncoder(snapshot).as_xml()


def DeleteDBInstance(*, DBInstanceIdentifier, **command):
    skip_snapshot = command.get('SkipFinalSnapshot') == 'true'
    snapshot_identifier = command.get('FinalDBSnapshotIdentifier')
    if not current_app.has_snapshots:
        skip_snapshot = True

    instance = get_instance(DBInstanceIdentifier)

    if 'creating' == instance.status and not skip_snapshot:
        raise errors.InvalidDBInstanceState(
            f"Instance {DBInstanceIdentifier} is currently creating "
            "- a final snapshot cannot be taken.")

    if not snapshot_identifier and not skip_snapshot:
        raise errors.InvalidParameterCombination(
            "FinalDBSnapshotIdentifier is required unless SkipFinalSnapshot "
            "is specified.")

    if instance.data.get('DeletionProtection', False):
        raise errors.InvalidParameterCombination(
            "Cannot delete protected DB Instance, please disable deletion "
            "protection and try again.")

    snapshot_id = None
    if not skip_snapshot:
        snapshot = DBSnapshot.factory(
            instance, 'manual', snapshot_identifier)
        db.session.add(snapshot)
        try:
            db.session.commit()
        except IntegrityError:
            raise errors.DBSnapshotAlreadyExists(snapshot_identifier)
        snapshot_id = snapshot.id
    instance.status = 'deleting'
    db.session.commit()

    worker.delete_db_instance.send(instance.id, snapshot_id)

    return xml.InstanceEncoder(instance).as_xml()


def DeleteDBSnapshot(*, DBSnapshotIdentifier, **command):
    snapshot = get_snapshot(DBSnapshotIdentifier)
    if snapshot.status not in ('available', 'failed'):
        raise errors.InvalidDBSnapshotState(
            "Cannot delete the snapshot because it is not currently in the "
            "available or failed state.",
        )

    snapshot.status = 'deleted'
    worker.delete_db_snapshot.send(snapshot.id)
    db.session.commit()
    return xml.SnapshotEncoder(snapshot).as_xml()


def DescribeDBInstances(**command):
    qry = DBInstance.query
    if 'DBInstanceIdentifier' in command:
        qry = qry.filter(
            DBInstance.identifier == command['DBInstanceIdentifier'])
    instances = qry.order_by(DBInstance.identifier).all()
    return xml.INSTANCE_LIST_TMPL.render(
        instances=[xml.InstanceEncoder(i) for i in instances])


def DescribeDBSnapshots(**command):
    qry = DBSnapshot.query
    if 'DBSnapshotIdentifier' in command:
        qry = qry.filter(
            DBSnapshot.identifier == command['DBSnapshotIdentifier'])
    snapshots = qry.order_by(DBSnapshot.identifier).all()
    return xml.SNAPSHOT_LIST_TMPL.render(
        snapshots=[xml.SnapshotEncoder(s) for s in snapshots])


def RebootDBInstance(*, DBInstanceIdentifier):
    instance = (
        DBInstance.query
        .filter(DBInstance.identifier == DBInstanceIdentifier)
        .one())
    instance.status = 'rebooting'
    db.session.commit()
    worker.reboot_db_instance.send(instance.id)
    return xml.InstanceEncoder(instance).as_xml()


def RestoreDBInstanceFromDBSnapshot(**command):
    snapshot = get_snapshot(
        command['DBSnapshotIdentifier'], status='available')
    identifier = check_instance_identifier(command['DBInstanceIdentifier'])
    instance = DBInstance.factory(identifier, extra=snapshot.data)
    instance.data.update(command)
    instance.recovery_token = str(uuid4())
    recovery_end_callback = instance.recovery_end_callback

    db.session.add(instance)
    try:
        db.session.commit()
    except IntegrityError:
        raise errors.DBInstanceAlreadyExists()

    worker.restore_db_instance_from_db_snapshot.send(
        instance.id, snapshot.id,
        recovery_end_callback=recovery_end_callback,
    )

    return xml.InstanceEncoder(instance).as_xml()


def RestoreDBInstanceToPointInTime(
        *, SourceDBInstanceIdentifier, TargetDBInstanceIdentifier,
        RestoreTime=None, UseLatestRestorableTime='false', **command):
    source = get_instance(SourceDBInstanceIdentifier)

    UseLatestRestorableTime = UseLatestRestorableTime == 'true'
    if UseLatestRestorableTime:
        RestoreTime = None
    elif RestoreTime is None:
        raise errors.InvalidParameterCombination(
            "If UseLatestRestoreTime is not true, "
            "the RestoreTime parameter must be specified."
        )

    data = dict(source.data, **command)
    del data['DBInstanceIdentifier']
    del data['DeletionProtection']
    identifier = check_instance_identifier(TargetDBInstanceIdentifier)
    target = DBInstance.factory(identifier, extra=data)
    target.recovery_token = str(uuid4())
    recovery_end_callback = target.recovery_end_callback
    source.data.pop('Endpoint', 'ignore-missing')

    db.session.add(target)
    try:
        db.session.commit()
    except IntegrityError:
        logger.exception('pouet')
        raise errors.DBInstanceAlreadyExists()

    worker.restore_db_instance_to_point_in_time.send(
        target.id, source.id, RestoreTime,
        recovery_end_callback=recovery_end_callback,
    )

    return xml.InstanceEncoder(target).as_xml()


def StartDBInstance(*, DBInstanceIdentifier):
    instance = get_instance(DBInstanceIdentifier, status='stopped')
    instance.status = 'starting'
    db.session.commit()
    worker.start_db_instance.send(instance.id)
    return xml.InstanceEncoder(instance).as_xml()


def StopDBInstance(DBInstanceIdentifier):
    instance = get_instance(DBInstanceIdentifier, status='available')
    instance.status = 'stopping'
    db.session.commit()
    worker.stop_db_instance.send(instance.id)
    return xml.InstanceEncoder(instance).as_xml()


def check_instance_identifier(identifier, attr='DBInstanceIdentifier'):
    try:
        return DBInstance.check_identifier(identifier)
    except ValueError:
        raise errors.InvalidParameterValue(heredoc(f"""\
        The parameter {attr} is not a valid identifier.
        Identifiers must begin with a letter; must contain only ASCII letters,
        digits, and hyphens; and must not end with a hyphen or contain two
        consecutive hyphens.
        """))


def check_snapshot_identifier(identifier):
    return check_instance_identifier(identifier, attr='DBSnapshotIdentifier')


def check_status(obj, status='available', msg=None):
    if status is None:
        return
    if isinstance(status, str):
        status = status,
    if obj.status not in status:
        cls = getattr(errors, f"Invalid{obj.__class__.__name__}State")
        if msg is None:
            msg = (
                f"{obj.__class__.__name__} must have state {status} but "
                f"actually has {obj.status}"
            )
        raise cls(msg)
