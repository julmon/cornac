from contextlib import contextmanager
from datetime import datetime
import re

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import (
    ENUM,
    JSONB,
)
from sqlalchemy_json import NestedMutable

from ..utils import canonical_url_for, format_time

db = SQLAlchemy()
# Our jsonb columns are subject to change and thus, sqlalchemy needs to track
# updates deep in the json. This add nested tracking of mutation of JSON data
# in tuple. See https://github.com/edelooff/sqlalchemy-json/issues/13.
JSONB = NestedMutable.as_mutable(JSONB)


@contextmanager
def connect(connstring):
    # Manager for connecting to psycopg2 without flask nor SQLAlchemy.
    import psycopg2
    conn = psycopg2.connect(connstring)
    try:
        yield conn
    finally:
        conn.close()


DBInstanceStatus = ENUM(
    # Keep it sync with cornac/core/schema/001-instances.sql.
    'available',
    'backing-up',
    'creating',
    'deleting',
    'failed',
    'incompatible-network',
    'incompatible-option-group',
    'incompatible-parameters',
    'incompatible-restore',
    'maintenance',
    'modifying',
    'rebooting',
    'renaming',
    'resetting-master-credentials',
    'restore-error',
    'starting',
    'stopped',
    'stopping',
    'storage-full',
    'storage-optimization',
    'upgrading',
    name='db_instance_status',
)


class DBInstance(db.Model):
    __tablename__ = 'db_instances'

    id = db.Column(db.Integer, primary_key=True)
    identifier = db.Column(db.String)
    status = db.Column(DBInstanceStatus)
    status_message = db.Column(db.String)
    recovery_token = db.Column(db.String)
    data = db.Column(JSONB)
    iaas_data = db.Column(JSONB)
    operator_data = db.Column(JSONB)

    _identifier_re = re.compile(r'[a-z][a-z0-9-]*[a-z0-9]')

    @classmethod
    def check_identifier(cls, value):
        if '--' in value:
            raise ValueError("Double hyphen detected")
        value = value.lower()
        if not cls._identifier_re.match(value):
            raise ValueError("Invalid identifier")
        return value

    @classmethod
    def factory(cls, identifier, extra=None):
        self = cls()
        self.identifier = identifier
        self.status = 'creating'
        self.data = dict(
            DBInstanceIdentifier=identifier,
            DeletionProtection=False,
            Engine='postgres',
            EngineVersion='11',
            MultiAZ=False,
        )
        if extra:
            self.data.update(extra)
        self.data.update(dict(
            InstanceCreateTime=format_time(),
        ))
        return self

    def __str__(self):
        return f'instance #{self.id} {self.identifier} ({self.status})'

    @property
    def recovery_end_callback(self):
        try:
            return self._recovery_end_callback
        except AttributeError:
            return canonical_url_for(
                'cornac.recovery_end_callback',
                _external=True,
                token=self.recovery_token,
            )

    @recovery_end_callback.setter
    def recovery_end_callback(self, value):
        self._recovery_end_callback = value

    def make_snapshot_identifier(self, date=None):
        if date is None:
            date = datetime.utcnow()
        return f'rds:{self.identifier}-{date:%Y-%m-%d-%H-%M}'


DBSnapshotStatus = ENUM(
    # Keep it sync with cornac/core/schema/003-snapshots.sql.
    'available',
    'creating',
    'deleted',
    'failed',
    name='db_instance_status',
)


# Keep it sync with cornac/core/schema/003-snapshots.sql.
DBSnapshotType = ENUM('automated', 'manual', name='db_snapshot_type')


class DBSnapshot(db.Model):
    __tablename__ = 'db_snapshots'

    id = db.Column(db.Integer, primary_key=True)
    identifier = db.Column(db.String)
    status = db.Column(DBSnapshotStatus)
    status_message = db.Column(db.String)
    instance_id = db.Column(db.Integer, db.ForeignKey('db_instances.id'))
    data = db.Column(JSONB)
    type_ = db.Column('type', DBSnapshotType)
    iaas_data = db.Column(JSONB)
    operator_data = db.Column(JSONB)

    instance = db.relationship(
        'DBInstance',
        # Always load corresponding instance of a snapshot, using a join.
        lazy='joined',
        # Reference all snapshots of an instance, but don't load them until
        # accessed.
        backref=db.backref('snapshots', lazy='select'),
    )

    def __str__(self):
        return f'snapshot #{self.id} {self.identifier}'

    _instance_attr_whitelist = {
        'AllocatedStorage',
        'DBInstanceIdentifier',
        'Engine',
        'EngineVersion',
        'InstanceCreateTime',
        'MasterUsername',
        'StorageType',
    }

    @classmethod
    def factory(cls, instance, type_, identifier=None):
        self = cls()
        self.status = 'creating'
        self.instance = instance
        self.type_ = type_
        if not identifier:
            identifier = instance.make_snapshot_identifier()
        self.identifier = identifier
        self.data = dict(
            {
                k: v for k, v in instance.data.items()
                if k in cls._instance_attr_whitelist
            },
            SnapshotCreateTime=format_time(),
            PercentProgress=0,
            XDBInstanceId=instance.id,
            Port=5432,
        )
        return self

    @property
    def siblings(self):
        cls = self.__class__
        return (
            cls.query
            .filter(cls.instance_id == self.instance_id)
            .filter(cls.id != self.id)
        )
