CREATE TYPE "db_snapshot_status" AS ENUM (
  -- Keep it sync with cornac/core/model.py.
  'available',
  'creating',
  'deleted',
  'failed'
);

CREATE TYPE "db_snapshot_type" AS ENUM (
  -- Keep it sync with cornac/core/model.py.
  'automated',
  'manual'
);

CREATE TABLE db_snapshots (
  id BIGSERIAL PRIMARY KEY,
  identifier TEXT UNIQUE,
  status db_snapshot_status NOT NULL,
  status_message TEXT,
  instance_id INTEGER REFERENCES db_instances(id),
  "type" db_snapshot_type NOT NULL,
  "data" JSONb,
  "iaas_data" JSONb,
  "operator_data" JSONb
);

ALTER TABLE db_instances
  ADD COLUMN IF NOT EXISTS recovery_token TEXT UNIQUE;

CREATE INDEX ON db_snapshots(instance_id);
